#include "index/index_iterator.h"

#include "index/basic_comparator.h"
#include "index/generic_key.h"

IndexIterator::IndexIterator() = default;

IndexIterator::IndexIterator(page_id_t page_id, BufferPoolManager *bpm, int index)
    : current_page_id(page_id), item_index(index), buffer_pool_manager(bpm) {
  page = reinterpret_cast<LeafPage *>(buffer_pool_manager->FetchPage(current_page_id)->GetData());
}

IndexIterator::~IndexIterator() {
  if (current_page_id != INVALID_PAGE_ID)
    buffer_pool_manager->UnpinPage(current_page_id, false);
}

std::pair<GenericKey *, RowId> IndexIterator::operator*() {
  std::pair<GenericKey *, RowId> result;
  result.first=page->KeyAt(item_index);
  result.second=page->ValueAt(item_index);
  return result;
}

IndexIterator &IndexIterator::operator++() {
  if (item_index == page->GetSize() - 1&& page->GetNextPageId() != INVALID_PAGE_ID)
  {
    Page* next_page = buffer_pool_manager->FetchPage(page->GetNextPageId());
    Page* cur_page = buffer_pool_manager->FetchPage(current_page_id);
    //next_page->RLatch();
    //cur_page->RUnlatch();
    buffer_pool_manager->UnpinPage(cur_page->GetPageId(), false);
    cur_page = next_page;
    page = reinterpret_cast<LeafPage *>(cur_page->GetData());
    item_index = 0;
  }
  else{
    item_index++;
  }
  return *this;
}

bool IndexIterator::operator==(const IndexIterator &itr) const {
  return current_page_id == itr.current_page_id && item_index == itr.item_index;
}

bool IndexIterator::operator!=(const IndexIterator &itr) const {
  return !(*this == itr);
}
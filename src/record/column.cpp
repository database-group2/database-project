#include "record/column.h"

#include "glog/logging.h"

Column::Column(std::string column_name, TypeId type, uint32_t index, bool nullable, bool unique)
    : name_(std::move(column_name)), type_(type), table_ind_(index), nullable_(nullable), unique_(unique) {
  ASSERT(type != TypeId::kTypeChar, "Wrong constructor for CHAR type.");
  switch (type) {
    case TypeId::kTypeInt:
      len_ = sizeof(int32_t);
      break;
    case TypeId::kTypeFloat:
      len_ = sizeof(float_t);
      break;
    default:
      ASSERT(false, "Unsupported column type.");
  }
}

Column::Column(std::string column_name, TypeId type, uint32_t length, uint32_t index, bool nullable, bool unique)
    : name_(std::move(column_name)),
      type_(type),
      len_(length),
      table_ind_(index),
      nullable_(nullable),
      unique_(unique) {
  ASSERT(type == TypeId::kTypeChar, "Wrong constructor for non-VARCHAR type.");
}

Column::Column(const Column *other)
    : name_(other->name_),
      type_(other->type_),
      len_(other->len_),
      table_ind_(other->table_ind_),
      nullable_(other->nullable_),
      unique_(other->unique_) {}

/**
* TODO: Student Implement
*/
uint32_t Column::SerializeTo(char *buf) const {
  uint32_t length=0;
  MACH_WRITE_TO(uint32_t, buf, COLUMN_MAGIC_NUM);
  buf+=4;
  MACH_WRITE_TO(TypeId, buf, type_);
  buf+=sizeof(TypeId);;
  MACH_WRITE_UINT32(buf, len_);
  buf+=4;
  uint32_t stringlen=0;
  stringlen=name_.size();
  MACH_WRITE_UINT32(buf, stringlen);
  buf+=4;
  memcpy(buf, name_.c_str(), name_.size());
  buf+=stringlen;
  MACH_WRITE_UINT32(buf, table_ind_);
  buf+=4;
  MACH_WRITE_TO(bool, buf , nullable_);
  buf+=1;
  MACH_WRITE_TO(bool, buf , unique_);
  buf+=1;
  length+= stringlen;
  length+= 18;
  length+= sizeof(TypeId);
  return length;
}

/**
 * TODO: Student Implement
 */
uint32_t Column::GetSerializedSize() const {
  uint32_t length=0;
  length+= name_.size();
  length+= 18;
  length+= sizeof(TypeId);
  return length;
}

/**
 * TODO: Student Implement
 */
uint32_t Column::DeserializeFrom(char *buf, Column *&column) {
  uint32_t magic_num = MACH_READ_FROM(uint32_t, buf);
  ASSERT(magic_num == COLUMN_MAGIC_NUM, "error:Column magic num is error");
  buf+=4;
  TypeId type = MACH_READ_FROM(TypeId, buf);
  buf+=sizeof(TypeId);
  uint32_t len=MACH_READ_UINT32(buf);
  buf+=4;
  uint32_t stringlen=MACH_READ_UINT32(buf);
  buf+=4;
  char name0[stringlen+1];
  memset(name0, '\0', sizeof(name0));
  memcpy(name0, buf, stringlen);
  std::string name(name0);
  buf+=stringlen;
  uint32_t table_ind=MACH_READ_UINT32(buf);
  buf+=4;
  bool nullable=MACH_READ_FROM(bool, buf);
  buf+=1;
  bool unique=MACH_READ_FROM(bool, buf);
  buf+=1;
  if (type != kTypeChar){
    column= new Column(name, type, table_ind, nullable, unique);
  }else{
    column= new Column(name, type, len, table_ind, nullable, unique);
  }
  
  uint32_t length=0;
  length+= stringlen;
  length+= 18;
  length+= sizeof(TypeId);
  return length;
}

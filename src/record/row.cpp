#include "record/row.h"

/**
 * TODO: Student Implement
 */
uint32_t Row::SerializeTo(char *buf, Schema *schema) const {
  ASSERT(schema != nullptr, "Invalid schema before serialize.");
  ASSERT(schema->GetColumnCount() == fields_.size(), "Fields size do not match schema's column size.");
  uint32_t length=0,nullnum=0;
  for (uint32_t i = 0; i < fields_.size(); i++) {
    if (fields_[i]->IsNull()) {
      nullnum++;
    }
  }
  MACH_WRITE_TO(uint32_t, buf, fields_.size());
  buf += 4;
  length+= 4;
  MACH_WRITE_TO(uint32_t, buf , nullnum);
  buf += 4;
  length+= 4;
  for (uint32_t i = 0; i < fields_.size(); i++) {
    if (fields_[i]->IsNull()) {
      MACH_WRITE_INT32(buf , i);
      buf += 4;
      length+= 4;
    }
  }
  uint32_t tmp;
  for (auto &itr : fields_) {
    if (!itr->IsNull()) tmp= itr->SerializeTo(buf);
    buf+=tmp;
    length+=tmp;
  }
  return length;
}

uint32_t Row::DeserializeFrom(char *buf, Schema *schema) {
  ASSERT(schema != nullptr, "Invalid schema before serialize.");
  ASSERT(fields_.empty(), "Non empty field in row.");

  uint32_t i = 0,length=0;
  uint32_t fieldnum = MACH_READ_UINT32(buf);
  buf += 4;
  length+=4;
  uint32_t nullnum = MACH_READ_UINT32(buf);
  buf += 4;
  length+=4;
  std::vector<uint32_t> null_bitmap(fieldnum, 0);
  for (i = 0; i < nullnum; i++) {
    null_bitmap[MACH_READ_UINT32(buf)] = 1;
    buf += 4;
    length+=4;
  }
  uint32_t tmp;
  for (i = 0; i < fieldnum; i++) {
    fields_.push_back(new Field(schema->GetColumn(i)->GetType()));
    if (!null_bitmap[i]) {
      tmp = Field::DeserializeFrom(buf , schema->GetColumn(i)->GetType(), &fields_[i], false);
      buf+=tmp;
      length+=tmp;
    }
  }
  return length;
}

uint32_t Row::GetSerializedSize(Schema *schema) const {
  ASSERT(schema != nullptr, "Invalid schema before serialize.");
  ASSERT(schema->GetColumnCount() == fields_.size(), "Fields size do not match schema's column size.");
  uint32_t length=0;
  length+= 8;
  for (uint32_t i = 0; i < fields_.size(); i++) {
    if (fields_[i]->IsNull()) {
      length+= 4;
    }
  }
  uint32_t tmp;
  for (auto &itr : fields_) {
    if (!itr->IsNull()) tmp= itr->GetSerializedSize();
    length+=tmp;
  }
  return length;
}

void Row::GetKeyFromRow(const Schema *schema, const Schema *key_schema, Row &key_row) {
  auto columns = key_schema->GetColumns();
  std::vector<Field> fields;
  uint32_t idx;
  for (auto column : columns) {
    schema->GetColumnIndex(column->GetName(), idx);
    fields.emplace_back(*this->GetField(idx));
  }
  key_row = Row(fields);
}

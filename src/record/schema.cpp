#include "record/schema.h"

/**
 * TODO: Student Implement
 */
uint32_t Schema::SerializeTo(char *buf) const {
  uint32_t num=this->GetColumnCount();
  uint32_t length=0;
  MACH_WRITE_TO(uint32_t, buf, SCHEMA_MAGIC_NUM);
  buf += 4;
  MACH_WRITE_INT32(buf, this->GetColumnCount());
  buf += 4;
  for(int i=0;i<num;i++){
    length+=columns_[i]->GetSerializedSize();
    columns_[i]->SerializeTo(buf);
  }
  buf+=length;
  MACH_WRITE_TO(bool, buf , is_manage_);
  buf+=1;
  length+=9;
  return length;
}

uint32_t Schema::GetSerializedSize() const {
  uint32_t num=this->GetColumnCount();
  uint32_t length=0;
  for(int i=0;i<num;i++){
    length+=columns_[i]->GetSerializedSize();
  }
  length+=9;
  return length;
}

uint32_t Schema::DeserializeFrom(char *buf, Schema *&schema) {
  uint32_t snum = MACH_READ_FROM(uint32_t, buf);
  ASSERT(snum == Schema::SCHEMA_MAGIC_NUM, "error:Schema magic num is error!");
  buf+=4;
  uint32_t num=MACH_READ_INT32(buf);
  buf+=4;
  uint32_t length=0;
  std::vector<Column *> columns_;
 // printf("%d\n",num);
  for(int i=0;i<num;i++){
    Column* newcolumn = nullptr;
    length+=Column::DeserializeFrom(buf, newcolumn);
    columns_.push_back(newcolumn);
  }
  bool manage=MACH_READ_FROM(bool, buf);
  buf+=1;
  length+=9;
  schema=new Schema(columns_,manage);
  return length;
}
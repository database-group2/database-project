#include "executor/executors/index_scan_executor.h"
/**
* TODO: Student Implement
*/
IndexScanExecutor::IndexScanExecutor(ExecuteContext *exec_ctx, const IndexScanPlanNode *plan)
    : AbstractExecutor(exec_ctx), plan_(plan) {}

void IndexScanExecutor::Init() {
   LOG(INFO) << "index scan starts";
   auto table_name = plan_->GetTableName();
   auto filter_predicate = plan_->GetPredicate();
   CatalogManager *catalog = exec_ctx_->GetCatalog();
   IndexInfo *indexinfo1;
   TableInfo *tableinfo1;
   index_id_t indexoid;
   catalog->GetTable(table_name,tableinfo1);
}

bool IndexScanExecutor::Next(Row *row, RowId *rid) {
  return false;
}

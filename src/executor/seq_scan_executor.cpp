//
// Created by njz on 2023/1/17.
//
#include "executor/executors/seq_scan_executor.h"
#include "executor/execute_context.h"
#include "catalog/catalog.h"
#include "record/row.h"

/**
* TODO: Student Implement
*/
SeqScanExecutor::SeqScanExecutor(ExecuteContext *exec_ctx, const SeqScanPlanNode *plan)
    : AbstractExecutor(exec_ctx),
      plan_(plan){}

void SeqScanExecutor::Init() {
  auto table_name = plan_->GetTableName();
  auto filter_predicate = plan_->GetPredicate();
  CatalogManager *catalog = exec_ctx_->GetCatalog();
  TableInfo *tableinfo1;
  catalog->GetTable(table_name,tableinfo1);
  TableHeap *tableheap = tableinfo1->GetTableHeap();
  TableIterator iter = tableheap->Begin(exec_ctx_->GetTransaction());
  Field truevalue(kTypeInt,1);
  while( iter != tableheap->End())
  {
    const Row &crow = *iter;
    const RowId &cid = crow.GetRowId();
    if(filter_predicate == nullptr || filter_predicate->Evaluate(&crow).CompareEquals(truevalue) == CmpBool::kTrue)
       value1_.push_back(std::pair<Row, RowId>(crow,cid));
    ++iter;
  }
  cur = 0;
}

bool SeqScanExecutor::Next(Row *row, RowId *rid) {
  if( cur>= value1_.size())
  {
    LOG(INFO) << "empty vector" <<endl;
    return false;
  }
  *row = value1_[cur].first;
  *row = value1_[cur].second;
  ++cur;
  LOG(INFO) << "POP FINISH" <<endl;
  return true;
}

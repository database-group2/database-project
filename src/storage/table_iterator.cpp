#include "storage/table_iterator.h"
#include "common/macros.h"
#include "storage/table_heap.h"

/**
 * TODO: Student Implement
 */
TableIterator::TableIterator(TableHeap *table_heap, RowId rid,Transaction*txn){
  this->row=new Row(rid);
  this->table_heap=table_heap;
  if (rid.GetPageId() != INVALID_PAGE_ID) {
    this->table_heap->GetTuple(row, txn);
  }
}

TableIterator::TableIterator(const TableIterator &other){
  this->row=other.row;
  this->table_heap=other.table_heap;
  this->txn=other.txn;
}

TableIterator::~TableIterator() {
  delete row;
}

bool TableIterator::operator==(const TableIterator &itr) const {
  if(row->GetRowId().Get() == itr.row->GetRowId().Get()){
    return true;
  }else {
    return false;
  }
}

bool TableIterator::operator!=(const TableIterator &itr) const {
  if(*this==itr){
    return false;
  }else {
    return true;
  }
}

const Row &TableIterator::operator*() {
  if(*this != table_heap->End()){
    return *row;
  }else{
    ASSERT(false, "error:out of range");
  }
}

Row *TableIterator::operator->() {
  if(*this != table_heap->End()){
    return row;
  }else{
    ASSERT(false, "error:out of range");
  }
}

TableIterator &TableIterator::operator=(const TableIterator &itr) noexcept {
  this->row=itr.row;
  this->table_heap=itr.table_heap;
  this->txn=itr.txn;
}

// ++iter
TableIterator &TableIterator::operator++() {
  BufferPoolManager *buf = table_heap->buffer_pool_manager_;
  TablePage * nowpage = (TablePage *)buf->FetchPage(row->GetRowId().GetPageId());
  nowpage->RLatch();
  assert(nowpage != nullptr);
  RowId next_tuple_rid;
  bool flag=nowpage->GetNextTupleRid(row->GetRowId(),&next_tuple_rid);
  if (flag == false) {
    while (nowpage->GetNextPageId() != INVALID_PAGE_ID) {
      TablePage * next_page = (TablePage *)buf->FetchPage(nowpage->GetNextPageId());
      nowpage->RUnlatch();
      buf->UnpinPage(nowpage->GetTablePageId(), false);
      nowpage = next_page;
      nowpage->RLatch();
      if (nowpage->GetFirstTupleRid(&next_tuple_rid)) {
        break;
      }
    }
  }
  row = new Row(next_tuple_rid);
  if (*this != table_heap->End()) {
    table_heap->GetTuple(row ,nullptr);
  }
  nowpage->RUnlatch();
  buf->UnpinPage(nowpage->GetTablePageId(), false);
  return *this;
}

// iter++
TableIterator TableIterator::operator++(int) {
  TableIterator newiter(*this);
  ++(*this);
  return newiter;
}

#include "storage/table_heap.h"

/**
 * TODO: Student Implement
 */
bool TableHeap::InsertTuple(Row &row, Transaction *txn) {
  if (row.GetSerializedSize(schema_) > TablePage::SIZE_MAX_ROW)return false;
  TablePage *nowpage = (TablePage *)buffer_pool_manager_->FetchPage(first_page_id_);
  if (nowpage == nullptr) return false;

  TablePage *newpage;
  page_id_t nextpage_id;
  bool insert_flag=nowpage->InsertTuple(row, schema_, txn, lock_manager_, log_manager_);
  while (!insert_flag) {
    nextpage_id = nowpage->GetNextPageId();
    if (nextpage_id == INVALID_PAGE_ID) {
      newpage = (TablePage *)buffer_pool_manager_->NewPage(nextpage_id);
      if (newpage == nullptr) {
        buffer_pool_manager_->UnpinPage(nowpage->GetTablePageId(), false);
        return false;
      } else {
        nowpage->SetNextPageId(nextpage_id);
        buffer_pool_manager_->UnpinPage(nowpage->GetTablePageId(), true);
        newpage->Init(nextpage_id, nowpage->GetTablePageId(), log_manager_, txn);
        nowpage = newpage;
        buffer_pool_manager_->UnpinPage(newpage->GetTablePageId(), true);
      }
    }else {
      buffer_pool_manager_->UnpinPage(nowpage->GetTablePageId(), false);
      nowpage = (TablePage *)buffer_pool_manager_->FetchPage(nextpage_id);
    }
    insert_flag=nowpage->InsertTuple(row, schema_, txn, lock_manager_, log_manager_);
  }
  buffer_pool_manager_->UnpinPage(nowpage->GetTablePageId(), true);
  return true;
}

bool TableHeap::MarkDelete(const RowId &rid, Transaction *txn) {
  // Find the page which contains the tuple.
  auto page = reinterpret_cast<TablePage *>(buffer_pool_manager_->FetchPage(rid.GetPageId()));
  // If the page could not be found, then abort the transaction.
  if (page == nullptr) {
    return false;
  }
  // Otherwise, mark the tuple as deleted.
  page->WLatch();
  page->MarkDelete(rid, txn, lock_manager_, log_manager_);
  page->WUnlatch();
  buffer_pool_manager_->UnpinPage(page->GetTablePageId(), true);
  return true;
}

/**
 * TODO: Student Implement
 */
bool TableHeap::UpdateTuple(const Row &row, const RowId &rid, Transaction *txn) {
  TablePage * page = (TablePage *)buffer_pool_manager_->FetchPage(rid.GetPageId());
  if (page == nullptr) return false;

  Row old_row(row);
  bool update_flag;
  switch (page->UpdateTuple(row, &old_row, schema_, txn, lock_manager_, log_manager_)) {
    case TablePage::RetState::ILLEGAL_CALL:
      buffer_pool_manager_->UnpinPage(page->GetTablePageId(), false);
      update_flag= false;
      break;
    case TablePage::RetState::INSUFFICIENT_TABLE_PAGE:
      MarkDelete(rid, txn);
      InsertTuple(old_row, txn);
      buffer_pool_manager_->UnpinPage(old_row.GetRowId().GetPageId(), true);
      update_flag= true;
      break;
    case TablePage::RetState::DOUBLE_DELETE:
      buffer_pool_manager_->UnpinPage(page->GetTablePageId(), false);
      update_flag= false;
      break;
    case TablePage::RetState::SUCCESS:
      buffer_pool_manager_->UnpinPage(page->GetTablePageId(), true);
      update_flag= true;
      break;
    default:
      update_flag= false;
  }
  return update_flag;
}

/**
 * TODO: Student Implement
 */
void TableHeap::ApplyDelete(const RowId &rid, Transaction *txn) {
  auto page = (TablePage *)buffer_pool_manager_->FetchPage(rid.GetPageId());
  ASSERT(page != nullptr, "Can not find the page");
  page->ApplyDelete(rid, txn, log_manager_);
  buffer_pool_manager_->UnpinPage(page->GetTablePageId(), true);
}

void TableHeap::RollbackDelete(const RowId &rid, Transaction *txn) {
  // Find the page which contains the tuple.
  auto page = reinterpret_cast<TablePage *>(buffer_pool_manager_->FetchPage(rid.GetPageId()));
  assert(page != nullptr);
  // Rollback to delete.
  page->WLatch();
  page->RollbackDelete(rid, txn, log_manager_);
  page->WUnlatch();
  buffer_pool_manager_->UnpinPage(page->GetTablePageId(), true);
}

/**
 * TODO: Student Implement
 */
bool TableHeap::GetTuple(Row *row, Transaction *txn) {
  auto page = (TablePage *)buffer_pool_manager_->FetchPage(row->GetRowId().GetPageId());
  if (page != nullptr){
    bool flag = page->GetTuple(row, schema_, txn, lock_manager_);
    buffer_pool_manager_->UnpinPage(row->GetRowId().GetPageId(), false);
    return flag;
  } else return false;
}

void TableHeap::DeleteTable(page_id_t page_id) {
  if (page_id != INVALID_PAGE_ID) {
    auto temp_table_page = reinterpret_cast<TablePage *>(buffer_pool_manager_->FetchPage(page_id));  // 删除table_heap
    if (temp_table_page->GetNextPageId() != INVALID_PAGE_ID)
      DeleteTable(temp_table_page->GetNextPageId());
    buffer_pool_manager_->UnpinPage(page_id, false);
    buffer_pool_manager_->DeletePage(page_id);
  } else {
    DeleteTable(first_page_id_);
  }
}

/**
 * TODO: Student Implement
 */
TableIterator TableHeap::Begin(Transaction *txn) {
  TablePage *page;
  page_id_t page_id = first_page_id_;
  RowId begin_row_id = INVALID_ROWID;
  while (page_id != INVALID_PAGE_ID) {
    page = (TablePage *)buffer_pool_manager_->FetchPage(page_id);
    bool flag=page->GetFirstTupleRid(&begin_row_id);
    if (flag) break;
    else page_id = page->GetNextPageId();
  }
  TableIterator itr(this,begin_row_id, txn);
  return itr;
}

/**
 * TODO: Student Implement
 */
TableIterator TableHeap::End() {
  RowId endrowid(INVALID_PAGE_ID,0);
  TableIterator itr(this,endrowid, nullptr);
  return itr;
}

#include "buffer/lru_replacer.h"

LRUReplacer::LRUReplacer(size_t num_pages):capacity(num_pages){}

LRUReplacer::~LRUReplacer() = default;

/**
 * TODO: Student Implement
 */
bool LRUReplacer::Victim(frame_id_t *frame_id) {
  if (deque.empty()) return false;
  auto least = deque.front(); 
  deque.pop_front();
  auto least_itr = map.find(least);
  if (least_itr != map.end()) {  
    map.erase(least_itr);  
    *frame_id = least;
    return true;
  }
  return false;
}

/**
 * TODO: Student Implement
 */
void LRUReplacer::Pin(frame_id_t frame_id) {
  auto least_itr = map.find(frame_id);
  if (least_itr != map.end()) {
    deque.erase(least_itr->second);
    map.erase(least_itr);
  }
}

/**
 * TODO: Student Implement
 */
void LRUReplacer::Unpin(frame_id_t frame_id) {
  if (map.size() >= capacity) return;
  auto itr = map.find(frame_id);
  if (itr != map.end()) return;
  deque.push_back(frame_id);
  map.insert(pair<frame_id_t, list<frame_id_t>::iterator>(frame_id, prev(deque.end(), 1)));
}

/**
 * TODO: Student Implement
 */
size_t LRUReplacer::Size() {
  return map.size();
}

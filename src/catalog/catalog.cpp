#include "catalog/catalog.h"
#include<algorithm>
#include<cstdio>

void CatalogMeta::SerializeTo(char *buf) const {
    ASSERT(GetSerializedSize() <= PAGE_SIZE, "Failed to serialize catalog metadata to disk.");
    MACH_WRITE_UINT32(buf, CATALOG_METADATA_MAGIC_NUM);
    buf += 4;
    MACH_WRITE_UINT32(buf, table_meta_pages_.size());
    buf += 4;
    MACH_WRITE_UINT32(buf, index_meta_pages_.size());
    buf += 4;
    for (auto iter : table_meta_pages_) {
        MACH_WRITE_TO(table_id_t, buf, iter.first);
        buf += 4;
        MACH_WRITE_TO(page_id_t, buf, iter.second);
        buf += 4;
    }
    for (auto iter : index_meta_pages_) {
        MACH_WRITE_TO(index_id_t, buf, iter.first);
        buf += 4;
        MACH_WRITE_TO(page_id_t, buf, iter.second);
        buf += 4;
    }
}

CatalogMeta *CatalogMeta::DeserializeFrom(char *buf) {
    // check valid
    uint32_t magic_num = MACH_READ_UINT32(buf);
    buf += 4;
    ASSERT(magic_num == CATALOG_METADATA_MAGIC_NUM, "Failed to deserialize catalog metadata from disk.");
    // get table and index nums
    uint32_t table_nums = MACH_READ_UINT32(buf);
    buf += 4;
    uint32_t index_nums = MACH_READ_UINT32(buf);
    buf += 4;
    // create metadata and read value
    CatalogMeta *meta = new CatalogMeta();
    for (uint32_t i = 0; i < table_nums; i++) {
        auto table_id = MACH_READ_FROM(table_id_t, buf);
        buf += 4;
        auto table_heap_page_id = MACH_READ_FROM(page_id_t, buf);
        buf += 4;
        meta->table_meta_pages_.emplace(table_id, table_heap_page_id);
    }
    for (uint32_t i = 0; i < index_nums; i++) {
        auto index_id = MACH_READ_FROM(index_id_t, buf);
        buf += 4;
        auto index_page_id = MACH_READ_FROM(page_id_t, buf);
        buf += 4;
        meta->index_meta_pages_.emplace(index_id, index_page_id);
    }
    return meta;
}

/**
 * TODO: Student Implement
 */
uint32_t CatalogMeta::GetSerializedSize() const {
 // ASSERT(false, "Not Implemented yet");
  uint32_t sum=12;
  sum = sum + table_meta_pages_.size()*(sizeof(table_id_t) + sizeof(page_id_t));
  sum = sum + index_meta_pages_.size()*(sizeof(index_id_t) + sizeof(page_id_t));
  return sum;
}

CatalogMeta::CatalogMeta() {}

/**
 * TODO: Student Implement
 */
CatalogManager::CatalogManager(BufferPoolManager *buffer_pool_manager, LockManager *lock_manager,
                               LogManager *log_manager, bool init)
    : buffer_pool_manager_(buffer_pool_manager), lock_manager_(lock_manager), log_manager_(log_manager) {
    if(init) {
        catalog_meta_ = new CatalogMeta();
        next_index_id_ = next_table_id_ = 0;
        table_names_ = std::unordered_map<std::string, table_id_t>();
        table_names_.clear();
        tables_ = std::unordered_map<table_id_t, TableInfo *>();
        tables_.clear();
        index_names_.clear();
        indexes_ = std::unordered_map<index_id_t, IndexInfo *>();
        indexes_.clear();
    } else {
        Page *page_ = buffer_pool_manager_->FetchPage(CATALOG_META_PAGE_ID);
        if (page_ == nullptr) {
           LOG(ERROR) << "Failed to fetch catalog metadata.";
           return;
        }
        catalog_meta_ = CatalogMeta::DeserializeFrom(page_->GetData());
        next_index_id_ = next_table_id_ = 0;
        std::map<table_id_t, page_id_t> table1 = *catalog_meta_->GetTableMetaPages();
        std::map<index_id_t, page_id_t> index1 = *catalog_meta_->GetIndexMetaPages();
        for (auto iter : table1) {
            LoadTable(iter.first, iter.second);
        }
        for (auto iter : index1) {
           LoadIndex(iter.first, iter.second);
        }
        buffer_pool_manager_->UnpinPage(CATALOG_META_PAGE_ID, false);
  }
    //ASSERT(false, "Not Implemented yet");
}

CatalogManager::~CatalogManager() {
  FlushCatalogMetaPage();
  delete catalog_meta_;
  for (auto iter : tables_) {
    delete iter.second;
  }
  for (auto iter : indexes_) {
    delete iter.second;
  }
}

/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::CreateTable(const string &table_name, TableSchema *schema,
                                    Transaction *txn, TableInfo *&table_info) {
  //check the existance;
  if( table_names_.count(table_name) > 0 )
  {
    LOG(ERROR) << table_name << " already exists";
    return DB_TABLE_ALREADY_EXIST;
  }
  table_id_t tid = next_table_id_;
  ++next_table_id_;
  page_id_t tablepageid;
  auto newpage = buffer_pool_manager_ ->NewPage(tablepageid);
  if (newpage == nullptr) {
    return DB_FAILED;
  }

  // Create a new TableMetadata
  TableMetadata *t_meta = TableMetadata::Create(tid,table_name,tablepageid,schema);

  // Create a new TableHeap
  TableHeap *t_heap = TableHeap::Create(buffer_pool_manager_,schema,txn,log_manager_,lock_manager_);
  
  // Create a new TableInfo
  table_info = TableInfo::Create();
  table_info->Init(t_meta,t_heap);
   
   //add the tableinfo 
   tables_.emplace(tid,table_info);

   //add the table name and ID
   table_names_.emplace(table_name, tid);
   buffer_pool_manager_->UnpinPage(tablepageid, true);
  
   //connect page_id,table_id
   catalog_meta_->table_meta_pages_.emplace(tid, tablepageid);
   auto catalog_meta_page = buffer_pool_manager_->FetchPage(CATALOG_META_PAGE_ID);
   catalog_meta_->SerializeTo(catalog_meta_page->GetData());
   buffer_pool_manager_->UnpinPage(CATALOG_META_PAGE_ID, true);
   //++CATALOG_META_PAGE_ID;
   

  return DB_SUCCESS;
}

/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::GetTable(const string &table_name, TableInfo *&table_info) {
  // if( table_names_.find(table_name) == table_names_.end() )
    // return DB_TABLE_NOT_EXIST;
   for( auto iter : table_names_ ) {
    if(iter.first == table_name) {
     table_info = tables_[iter.second];
     return DB_SUCCESS;
   }
  }
 // printf("yes5\n");
  return DB_TABLE_NOT_EXIST;
}

/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::GetTables(std::vector<TableInfo *> &tables) const {
  // ASSERT(false, "Not Implemented yet");
  if(tables_.empty()) return DB_TABLE_NOT_EXIST;
  for( auto iter : tables_) {
    tables.push_back(iter.second);
  }
  return DB_FAILED;
}

dberr_t CatalogManager::CreateIndex(const std::string &table_name, const string &index_name,
                                    const std::vector<std::string> &index_keys, Transaction *txn,
                                    IndexInfo *&index_info, const string &index_type) {
  // ASSERT(false, "Not Implemented yet");
  //check if table exists
  if( table_names_.count(table_name) <= 0 )
  {
    LOG(ERROR) << "Table " << table_name << " does not exist";
    return DB_TABLE_NOT_EXIST;
  }
  //check if the index exists
  if( index_names_[table_name].count(index_name) > 0 )
  {
    LOG(ERROR) << "Index " << index_name << " exists";
    return DB_INDEX_ALREADY_EXIST;
  }
  //Create a new index ID
  index_id_t inid = next_index_id_;
  ++next_index_id_;

  std::vector<uint32_t>vmap;
  TableInfo *tinfo;
  GetTable(table_name, tinfo);
  TableSchema *tschema = tinfo->GetSchema();
  for( auto &key : index_keys) {
     uint32_t cindex;
     dberr_t result = tschema->GetColumnIndex(key, cindex);
     if(result!=DB_SUCCESS)
     {
      LOG(ERROR) << "Column " << key << " does not exist";
      return result;
     }
     vmap.push_back(cindex);
  }
  //create a new indexMetadata
  IndexMetadata *inmeta;
  inmeta = IndexMetadata::Create(inid, index_name, tinfo->GetTableId(), vmap);
  
  IndexInfo *new_index_info = IndexInfo :: Create();
  new_index_info ->Init(inmeta, tinfo, buffer_pool_manager_ , vmap);
  page_id_t inpageid;
  auto newpage = buffer_pool_manager_->NewPage(inpageid);
  if(newpage == nullptr)
  {
    return DB_FAILED;
  }
  catalog_meta_->index_meta_pages_.emplace(inid , inpageid);
  auto catalog_meta_page = buffer_pool_manager_->FetchPage(CATALOG_META_PAGE_ID);
  catalog_meta_->SerializeTo(catalog_meta_page->GetData());
 // new_index_info->SetRootPageId(inpageid);

 // Add the IndexInfo object to the catalog
  indexes_.emplace(inid, new_index_info);

  // Add the index name and ID to the catalog
  index_names_[table_name].emplace(index_name, inid);

  // Set the output parameter
  index_info = new_index_info;

  return DB_SUCCESS;
}
/*
dberr_t CatalogManager::CreateIndex(const std::string &table_name, const std::string &index_name,
                                    const std::vector<std::string> &index_keys, Transaction *txn,
                                    IndexInfo *&index_info, const std::string &index_type) {
  // Get the TableInfo object for the table
  TableInfo *table_info;
  dberr_t result = GetTable(table_name, table_info);
  if (result != DB_SUCCESS) {
    // LOG(ERROR) << "Table " << table_name << " does not exist";
    return result;
  }

  // Check if the index already exists
  if (index_names_[table_name].count(index_name) > 0) {
    // LOG(ERROR) << "Index " << index_name << " already exists";
    return DB_INDEX_ALREADY_EXIST;
  }

  // Create a new index ID
  index_id_t index_id = next_index_id_++;

  // Create a new key_map according to the index_keys
  std::vector<uint32_t> key_map;
  const TableSchema *table_schema = table_info->GetSchema();
  for (const auto &key : index_keys) {
    uint32_t column_index = 0;
    dberr_t result = table_schema->GetColumnIndex(key, column_index);
    if (result != DB_SUCCESS) {
      //   LOG(ERROR) << "Column " << key << " does not exist in table " << table_name;
      return result;
    }
    key_map.push_back(column_index);
  }

  // Create a new IndexMetadata object
  IndexMetadata *index_meta = IndexMetadata::Create(index_id, index_name, table_info->GetTableId(), key_map);

  // Create a new IndexInfo object
  IndexInfo *new_index_info = IndexInfo::Create();
  new_index_info->Init(index_meta, table_info, buffer_pool_manager_, key_map);

  // Add the IndexInfo object to the catalog
  indexes_.emplace(index_id, new_index_info);

  // Add the index name and ID to the catalog
  index_names_[table_name].emplace(index_name, index_id);

  // Set the output parameter
  index_info = new_index_info;
  // yyzTODO: return index_type

  return DB_SUCCESS;
}*/

/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::GetIndex(const std::string &table_name, const std::string &index_name,
                                 IndexInfo *&index_info) const {
   if(index_names_.find(table_name)== index_names_.end())
   {
      return DB_TABLE_NOT_EXIST;
   }
   if (index_names_.at(table_name).find(index_name) == index_names_.at(table_name).end()) 
   {
      return DB_INDEX_NOT_FOUND;
   }
  index_info = indexes_.at(index_names_.at(table_name).at(index_name));
  return DB_SUCCESS;
}


/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::GetTableIndexes(const std::string &table_name, std::vector<IndexInfo *> &indexes) const {
  if (index_names_.find(table_name) == index_names_.end())
  {
     return DB_TABLE_NOT_EXIST;
  }
  if (index_names_.at(table_name).empty())
  {
     return DB_INDEX_NOT_FOUND;
  }
  auto index1 = index_names_.at(table_name);
  for( auto iter : index1)
  {
      indexes.push_back(indexes_.at(iter.second));
  }
}

/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::DropTable(const string &table_name) {
  // ASSERT(false, "Not Implemented yet");
  std::unordered_map<std::string, table_id_t>::iterator iter;
  iter = table_names_.find(table_name);
  if(iter == table_names_.end())
     return DB_TABLE_NOT_EXIST;
  table_id_t table_id = iter->second;
  auto index1=index_names_.find(table_name);
  if(index1 != index_names_.end())
  {
    auto index2 = index1->second;
    for (auto iter : index2)
    {
      auto index3 = iter.second;
      indexes_.erase(index3);
    }
    index_names_.erase(table_name);
  }
  table_names_.erase(table_name);
  tables_.erase(table_id);
  return DB_SUCCESS;
}
/**
 * TODO: Student Implement
 */

dberr_t CatalogManager::DropIndex(const string &table_name, const string &index_name) {
  auto index1=index_names_.find(table_name);
  int flag = 0;
  if(index1 != index_names_.end())
  {
    auto index2 = index1->second;
    for (auto iter : index2)
     if(iter.first == index_name)
     {
      auto indexid = iter.second;
      indexes_.erase(indexid);
      flag = 1;break;
     }
     if(!flag) return DB_INDEX_NOT_FOUND;
     index2.erase(index_name);
     return DB_SUCCESS;
  }
  return DB_TABLE_NOT_EXIST;
}

/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::FlushCatalogMetaPage() const {
  // empty catalog table_meta_page and index_meta_page
  catalog_meta_->GetTableMetaPages()->clear();
  catalog_meta_->GetIndexMetaPages()->clear();
  // Serialize all table metadata
  for (auto iter : tables_) {
    auto table_meta = iter.second->GetTablemeta();

    // Get the table metadata page
    page_id_t page_id;
    Page *page = buffer_pool_manager_->NewPage(page_id);
    if (page == nullptr) {
      return DB_FAILED;
    }

    table_meta->SerializeTo(page->GetData());
    buffer_pool_manager_->UnpinPage(page_id, true);
    catalog_meta_->GetTableMetaPages()->emplace(iter.second->GetTableId(), page_id);
  }

  // Serialize all index metadata
  for (auto iter : indexes_) {
    auto index_meta = iter.second->Getindexmeta();

    // Get the index metadata page
    page_id_t page_id;
    Page *page = buffer_pool_manager_->NewPage(page_id);
    if (page == nullptr) {
      return DB_FAILED;
    }

    // Write the serialized index metadata to the page
    index_meta->SerializeTo(page->GetData());
    // Unpin and delete the page and buffer
    buffer_pool_manager_->UnpinPage(page_id, true);
    catalog_meta_->GetIndexMetaPages()->emplace(index_meta->GetIndexId(), page_id);
  }

  // Serialize the catalog metadata

  // Get the catalog metadata page
  Page *page = buffer_pool_manager_->FetchPage(CATALOG_META_PAGE_ID);
  if (page == nullptr) {
    return DB_FAILED;
  }

  catalog_meta_->SerializeTo(page->GetData());
  buffer_pool_manager_->UnpinPage(CATALOG_META_PAGE_ID, true);
  buffer_pool_manager_->FlushPage(CATALOG_META_PAGE_ID);
  return DB_SUCCESS;
}

/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::LoadTable(const table_id_t table_id, const page_id_t page_id) {
  Page *page = buffer_pool_manager_->FetchPage(page_id);
  if (page == nullptr) {
    return DB_FAILED;
  }

  char *s= page->GetData();
  TableMetadata *tmetadata = nullptr;
  TableMetadata::DeserializeFrom(s,tmetadata);

  // Create a new TableInfo object
  TableHeap *table_heap = TableHeap::Create(buffer_pool_manager_, tmetadata->GetFirstPageId(),
                                           tmetadata->GetSchema(), log_manager_, lock_manager_);
  TableInfo *table_info = TableInfo::Create();
  table_info->Init(tmetadata, table_heap);
  tables_.emplace(table_id, table_info);
  table_names_.emplace(table_info->GetTableName(), table_id);
  index_names_.emplace(table_info->GetTableName(), std::unordered_map<std::string, index_id_t>());
  next_table_id_++;

  // Unpin the page
  buffer_pool_manager_->UnpinPage(page_id, false);
  return DB_SUCCESS;
}

/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::LoadIndex(const index_id_t index_id, const page_id_t page_id) {
  // ASSERT(false, "Not Implemented yet");
  Page *page = buffer_pool_manager_->FetchPage(page_id);
  if (page == nullptr) {
    return DB_FAILED;
  }
  char *s= page->GetData();
  IndexMetadata *imetadata = nullptr;
  IndexMetadata:: DeserializeFrom(s,imetadata);
   
  //get the table which the index belong to
  table_id_t tid = imetadata->GetTableId();
  TableInfo *table_info = TableInfo::Create();
  GetTable(tid,table_info);
  IndexInfo *index_info = IndexInfo::Create();

  //create indexinfo  
  std::vector<uint32_t> key_map = imetadata->GetKeyMapping();
  index_info->Init(imetadata, table_info, buffer_pool_manager_, key_map);
  
   // Add the IndexInfo object to the catalog
  indexes_.emplace(index_id, index_info);

   // Add the index name and ID to the catalog
  index_names_[table_info->GetTableName()].emplace(index_info->GetIndexName(), index_id);
  next_index_id_++;

   // Unpin the page
  buffer_pool_manager_->UnpinPage(page_id, false);
  //buffer_pool_manager_->UnpinPage(page_id, false);
  return DB_SUCCESS;
}

/**
 * TODO: Student Implement
 */
dberr_t CatalogManager::GetTable(const table_id_t table_id, TableInfo *&table_info) {
  // ASSERT(false, "Not Implemented yet");
  auto table = tables_.find(table_id);
  if (table == tables_.end()) return DB_TABLE_NOT_EXIST;
  table_info = table->second;
  return DB_FAILED;
}